# spark/hadoop/s3 image notes here: https://gitlab.com/calincs/infrastructure/spark-on-k8s/-/blob/master/History.md
# FROM mesosphere/spark:spark-3.0.0-hadoop-2.9-k8s-rc2
FROM mesosphere/spark:spark-3.1.1-hadoop-2.9-k8s

# Add dependencies for hadoop-aws and S3
ADD https://repo1.maven.org/maven2/org/apache/hadoop/hadoop-aws/3.3.2/hadoop-aws-3.3.2.jar $SPARK_HOME/jars
ADD https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk/1.11.1026/aws-java-sdk-1.11.1026.jar $SPARK_HOME/jars
ADD https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-core/1.11.1026/aws-java-sdk-core-1.11.1026.jar $SPARK_HOME/jars
ADD https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-dynamodb/1.11.1026/aws-java-sdk-dynamodb-1.11.1026.jar $SPARK_HOME/jars
ADD https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-kms/1.11.1026/aws-java-sdk-kms-1.11.1026.jar $SPARK_HOME/jars
ADD https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk-s3/1.11.1026/aws-java-sdk-s3-1.11.1026.jar $SPARK_HOME/jars

USER root

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8

RUN apt-get update && \
    apt-get install -yq --no-install-recommends \
    python3 \
    python3-dev \
    python3-pip \
    python3-wheel

# Clear
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN ln -s $(which python3) /usr/local/bin/python

RUN python3 -m pip --no-cache-dir install --upgrade \
    pip \
    setuptools

RUN python3 -m pip --no-cache-dir install --upgrade \
    pandas==1.4.4 \
    findspark \
    splink==3.4.2 \
    requests \
    wheel \
    pyspark==3.1.1

COPY ./scala-udf-similarity-0.0.9.jar /opt/spark/work-dir/
COPY ./graphframes-0.8.0-spark3.0-s_2.12.jar /opt/spark/work-dir/
COPY ./app /opt/spark/work-dir/python/app