from __future__ import print_function

from pyspark.sql import Window, types, SQLContext
from pyspark.sql.functions import desc, row_number, col, lit
from pyspark.sql.types import StructType
from settings import get_linking_settings
from blocking import register_udfs, blocking
from utils import get_authority_dataframe, get_job_dataframe
from normalize_data import register_normalization_udfs, df_lowercase, delete_extra_columns, add_missing_columns
from normalize_data import author_normalize, dates_normalize, gender_normalize, titles_normalize, common_data_normalize
from normalize_data import split_list_columns

from splink.spark.spark_linker import SparkLinker


# following two functions are to break the lineage of the spark process
# this basically saves the results from a step, and then starts a new job
# preventing long jobs from failing due to memory problems
def blocked_comparisons_to_disk(df, spark):
	df.write.mode("overwrite").parquet("S3a://users/recon-api/dev/data/gammas_temp/")
	df_new = spark.read.parquet("S3a://users/recon-api/dev/data/gammas_temp/")
	return df_new


def scored_comparisons_to_disk(df, spark):
	df.write.mode("overwrite").parquet("S3a://users/recon-api/dev/data/df_e_temp/")
	df_new = spark.read.parquet("S3a://users/recon-api/dev/data/df_e_temp/")
	return df_new


def get_output_headings(all_headers):
	# returns a dictionary mapping original column names to the names that should be returned to the user
	# only includes headings that we want to be output to the user so we do not output the records the user provided
	# input records end with _r. authority records end with _l.
	out_headers = {}
	remove_duplicates = []
	for header in all_headers:
		# authority columns ending with _l and not already in the output
		if header.endswith("_l") and header[:-2] not in remove_duplicates and header != "unique_id_l":
			out_headers[header] = header[:-2]
		# additional columns to keep without renaming
		elif header == "match_probability" or header == "unique_id":
			out_headers[header] = header

	return out_headers


def run_normalization(spark, target_kg, df_input):

	print("RUN NORMALIZATION INPUT: ")
	print("df_input: ", df_input)

	# Normalize Input Data

	# get user defined spark functions
	dfWhitespace, dfPunctuation, dfNameConcat, dfGender = register_normalization_udfs(spark)

	# split list columns into individual columns
	df_input_headers = df_input.schema.names
	df_input = split_list_columns(target_kg, df_input, df_input_headers)

	# delete all extra columns in the input file
	df_input_headers = df_input.schema.names
	df_input = delete_extra_columns(target_kg, df_input, df_input_headers)

	print("df_input_headers: ", df_input_headers)

	# find out what headers are provided already
	# this happens again after extra columns are deleted so that we don't think a header is present after deletion
	df_input_headers = df_input.schema.names

	# add null columns if one of the authority columns does not exist
	# this is the only way to have splink return all authority columns
	# TODO: test doing a join at the end instead
	df_input = add_missing_columns(target_kg, df_input, df_input_headers)

	# reverses first and last name and adds as column
	df_input = author_normalize(target_kg, df_input, df_input_headers, dfNameConcat)

	# takes year only
	df_input = dates_normalize(target_kg, df_input, df_input_headers)

	# split title1 on semicolon
	df_input = titles_normalize(target_kg, df_input, df_input_headers)

	# standardize the gender representation
	df_input = gender_normalize(target_kg, df_input, dfGender, df_input_headers)

	# lowercase, remove punctuation, remove extra whitespace for string columns
	# convert all empty cells to a true null
	# this should be the last cleaning step so that all nulls are converted
	df_input = common_data_normalize(target_kg, df_input, df_lowercase, dfWhitespace, dfPunctuation, df_input_headers)

	return df_input, df_input_headers


def run_linking(spark, args):

	# get system arguments passed in through the yml file
	job_id = args[0].split("_")[0]
	target_kg = args[1]
	input_format = args[2]
	match_number = args[3]
	if match_number == "" or match_number is None:
		match_number == "25"
	match_threshold = args[4]
	# don't allow less than 0.1 or it returns a very large number of irrelevant candidates
	# if float(match_threshold) < 0.1:
	# 	match_threshold = "0.1"
	s3_data_path = args[5]
	s3_authority_path = args[6]

	# if the target_kg ends in -sample
	# then it only impacts what version of authority data we use, which is already specified by the yaml file
	target_kg = target_kg.replace("-sample", "")

	# get input data
	df_input = get_job_dataframe(spark, job_id, input_format, s3_data_path)
	if df_input is False:
		print("invalid filetype")
		# TODO: upload a failure code to the output folder for the api to check

	df_input, df_input_headers = run_normalization(spark, target_kg, df_input)

#######

	# generate blocking key
	secLongestWord, longestWord, firstWord, lastWord = register_udfs(spark)
	df_input = blocking(target_kg, df_input, longestWord, secLongestWord, firstWord, lastWord)

	# get the dataframe of the specified authority file
	# TODO deal with wikidata author work logic
	df_authority = get_authority_dataframe(spark, s3_authority_path)
	df_authority_headers = df_authority.schema.names

	# print cleaned data to validate while testing
	print("job id")
	print(job_id)
	print("df input")
	print(df_input.show(20))
	print("df authority")
	print(df_authority.show(20))

	# # TODO generate blocking key
	authority_headers = df_authority.schema.names
	# get settings for each of these depending on the
	kg_setting = ""
	if "author_Name_1" in authority_headers:
		kg_setting = kg_setting + "a"
	else:
		kg_setting = kg_setting + "_"
	if "work_Name_1" in authority_headers:
		kg_setting = kg_setting + "w"
	else:
		kg_setting = kg_setting + "_"
	if "expression_Name_1" in authority_headers:
		kg_setting = kg_setting + "e"
	else:
		kg_setting = kg_setting + "_"

	print("df_input_headers:  ", df_input_headers)
	print("df_authority_headers:  ", df_authority_headers)
	settings = get_linking_settings(target_kg, kg_setting, df_input_headers, df_authority_headers)
	linker = SparkLinker(
		[df_input, df_authority],
		settings,
		break_lineage_method="persist")
	results = linker.predict()

	# TODO figure out how to skip the panda conversion
	panda_df = results.as_pandas_dataframe()

	# Can't create the spark
	if not panda_df.empty:
		df_out = spark.createDataFrame(panda_df)

		print("df out 1")
		print(df_out.show(20))

		#########

		# remove duplicates created from the multiple blocking keys
		df_out = df_out.dropDuplicates(["match_probability", "unique_id_l", "unique_id_r"])

		print("df out 2")
		print(df_out.show(20))

		window = Window.partitionBy("unique_id_l").orderBy(desc("match_probability"))

		# select the data we want to return to the user
		# organize it by input record id
		# only return records that meet the minimum matchThreshold and matchProbability values requested
		header_dict = {"match_probability": "match_probability", "unique_id_l": "unique_id", "unique_id_r": "unique_id_authority"}

		sqlDF = df_out.select(*((col(i).alias(header_dict[i])) for i in header_dict), row_number().over(window).alias('rownum')).filter((col('rownum') <= match_number) & (col('match_probability') >= match_threshold)).drop('rownum')

		# print result sample to validate while testing
		print("Final DF")
		print(sqlDF.show(10, False))

		return sqlDF

	else:
		print(f"There were no results for job {job_id}")

		# return empty dataframe
		#return spark.createDataFrame([], StructType([]))

		# return error dataframe
		# Todo could return actual error messages
		error_dict = {"error": "No Results"}
		df = spark.createDataFrame(error_dict.items(), ["error", "value"])
		return df
