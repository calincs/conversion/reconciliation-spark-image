import json
import splink.spark.spark_comparison_library as cl

# TODO: string normalize should include all columns used for reconciling
# TODO fix description
def get_headings(setting, target_kg):
	"""
	List of columns that need various normalizations for each authority file

	The "duplicate" headings are ones that will be duplicated, resulting in columnName and columnName_original.
	We duplicate them so that after we link the data using the normalized values,
	we can return the original value to the user

	The "normalize" headings are ones that will undergo general string cleaning
	(like lowercasing, remove punctuation, etc.)

	"""
	with open("/opt/spark/work-dir/python/app/authority_config/" + target_kg + ".json") as config:
		data = json.load(config)
	try:
		return data[setting]
	except KeyError as err:
		print(err)
		return []


def get_blocking_rules(kg_setting):
	"""
	kg_setting will contain any of the following three letters if the corresponding column is in the source data
	a for author_Name_1
	w for work_Name_1
	e for expression_Name_1
	"""

	print("KG SETTING in get blocking rules: ", kg_setting)

	# only author blocking
	if "a" in kg_setting and "e" not in kg_setting and "w" not in kg_setting:
		rules = [
			'l.ablock1 = r.ablock1',
			'l.ablock2 = r.ablock2'
		]
	elif "e" in kg_setting or "w" in kg_setting:
		# only title blocking
		if "a" not in kg_setting:
			rules = [
				'l.tblock1 = r.tblock1',
				'l.tblock2 = r.tblock2'
				]
		# title and author blocking
		else:
			rules = [
				'l.ablock1 = r.ablock1',
				'l.tblock1 = r.tblock1'
			]

	return rules


def get_comparison_columns(target_kg, input_headers):
	"""
	Output structure:
	[{"comparison_levels":[{"sql_condition":"", "is_null_level": true},{},...]},{},...]
	"""

	print("INPUT HEADERS:  ", input_headers)

	comparisons = []

	# for title comparison
	if "viaf-expressions" in target_kg:
		title1 = "expression_Name_1"
		title2 = "expression_Name_2"
	else:
		title1 = "work_Name_1"
		title2 = "work_Name_2"

	authorName1 = "author_Name_1"
	authorName2 = "author_Name_2"
	authorBirthYear = "author_BirthYear"
	authorDeathYear = "author_DeathYear"
	workPublicationDate = "work_PublicationDate"

	# Author Birth year and Death year comparisons
	if authorDeathYear in input_headers or authorBirthYear in input_headers:
		comparison_levels = [
			{
				"sql_condition": f"({authorBirthYear}_l = {authorBirthYear}_r AND {authorDeathYear}_l ={authorDeathYear}_r)\n OR ({authorBirthYear}_l = {authorBirthYear}_r AND {authorDeathYear}_l IS NULL)\n OR ({authorBirthYear}_l = {authorBirthYear}_r AND {authorDeathYear}_r IS NULL)\n OR ({authorDeathYear}_l = {authorDeathYear}_r AND {authorBirthYear}_l IS NULL)\n OR ({authorDeathYear}_l = {authorDeathYear}_r AND {authorBirthYear}_r IS NULL)"
			},
			{
				"sql_condition": "ELSE"
			}
			]

		comparisons.append({"comparison_levels": comparison_levels})

	# Work publication date year comparison
	if "wikidata-works" in target_kg:
		if workPublicationDate in input_headers:
			comparison_levels = [
				{
					"sql_condition": f"({workPublicationDate}_l = {workPublicationDate}_r)"
				},
				{
					"sql_condition": "ELSE"
				}
				]

			comparisons.append({"comparison_levels": comparison_levels})

	# title comparison
	if title1 in input_headers and title2 in input_headers:
		level1 = 0.94
		level2 = 0.88
		measure = "jaccard_sim"
		comparison_levels = [
			{
				"sql_condition": f"({title1}_l IS NULL AND {title2}_l IS NULL)\n OR ({title1}_r IS NULL AND {title2}_r IS NULL)",
				"is_null_level": True
			},
			{
				"sql_condition": f"(NOT {title1}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title1}_l, {title1}_r) > {level1})\n OR (NOT {title2}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title2}_l, {title1}_r) > {level1})\n OR (NOT {title1}_l IS NULL AND NOT {title2}_r IS NULL AND {measure}({title1}_l, {title2}_r) > {level1})\n OR (NOT {title2}_l IS NULL AND NOT {title2}_r IS NULL AND {measure}({title2}_l, {title2}_r) > {level1})"
			},
			{
				"sql_condition": f"(NOT {title1}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title1}_l, {title1}_r) > {level2})\n OR (NOT {title2}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title2}_l, {title1}_r) > {level2})\n OR (NOT {title1}_l IS NULL AND NOT {title2}_r IS NULL AND {measure}({title1}_l, {title2}_r) > {level2})\n OR (NOT {title2}_l IS NULL AND NOT {title2}_r IS NULL AND {measure}({title2}_l, {title2}_r) > {level2})"
			},
			{
				"sql_condition": "ELSE"
			}]

		comparisons.append({"comparison_levels": comparison_levels})

	elif title1 in input_headers and title2 not in input_headers:
		level1 = 0.94
		level2 = 0.88
		measure = "jaccard_sim"
		comparison_levels = [
			{
				"sql_condition": f"({title1}_l IS NULL AND {title2}_l IS NULL)\n OR ({title1}_r IS NULL AND {title2}_r IS NULL)",
				"is_null_level": True
			},
			{
				"sql_condition": f"(NOT {title1}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title1}_l, {title1}_r) > 0.94)\n OR (NOT {title2}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title2}_l, {title1}_r) > 0.94)"
			},
			{
				"sql_condition": f"(NOT {title1}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title1}_l, {title1}_r) > 0.88)\n OR (NOT {title2}_l IS NULL AND NOT {title1}_r IS NULL AND {measure}({title2}_l, {title1}_r) > 0.88)"
			},
			{
				"sql_condition": "ELSE"
			}]

		comparisons.append({"comparison_levels": comparison_levels})

	# author name comparisons
	if authorName1 in input_headers:
		level1 = 0.94
		level2 = 0.88
		level3 = 0.84
		measure = "jaro_winkler_sim"
		comparison_levels = [
			{
				"sql_condition": f"({authorName1}_l IS NULL AND {authorName2}_l IS NULL)\n OR ({authorName1}_r IS NULL AND {authorName2}_r IS NULL)",
				"is_null_level": True
			},
			{
				"sql_condition": f"(NOT {authorName1}_l IS NULL AND NOT {authorName1}_r IS NULL AND {measure}({authorName1}_l, {authorName1}_r) > {level1})\n OR (NOT {authorName2}_l IS NULL AND NOT {authorName1}_r IS NULL AND {measure}({authorName2}_l, {authorName1}_r) > {level1})\n OR (NOT {authorName1}_l IS NULL AND NOT {authorName2}_r IS NULL AND {measure}({authorName1}_l, {authorName2}_r) > {level1})\n OR (NOT {authorName2}_l IS NULL AND NOT {authorName2}_r IS NULL AND {measure}({authorName2}_l, {authorName2}_r) > {level1})"
			},
			{
				"sql_condition": f"(NOT {authorName1}_l IS NULL AND NOT {authorName1}_r IS NULL AND {measure}({authorName1}_l, {authorName1}_r) > {level2})\n OR (NOT {authorName2}_l IS NULL AND NOT {authorName1}_r IS NULL AND {measure}({authorName2}_l, {authorName1}_r) > {level2})\n OR (NOT {authorName1}_l IS NULL AND NOT {authorName2}_r IS NULL AND {measure}({authorName1}_l, {authorName2}_r) > {level2})\n OR (NOT {authorName2}_l IS NULL AND NOT {authorName2}_r IS NULL AND {measure}({authorName2}_l, {authorName2}_r) > {level2})"
			},
			{
				"sql_condition": f"(NOT {authorName1}_l IS NULL AND NOT {authorName1}_r IS NULL AND {measure}({authorName1}_l, {authorName1}_r) > {level3})\n OR (NOT {authorName2}_l IS NULL AND NOT {authorName1}_r IS NULL AND {measure}({authorName2}_l, {authorName1}_r) > {level3})\n OR (NOT {authorName1}_l IS NULL AND NOT {authorName2}_r IS NULL AND {measure}({authorName1}_l, {authorName2}_r) > {level3})\n OR (NOT {authorName2}_l IS NULL AND NOT {authorName2}_r IS NULL AND {measure}({authorName2}_l, {authorName2}_r) > {level3})"
			},
			{
				"sql_condition": "ELSE"
			}]

		comparisons.append({"comparison_levels": comparison_levels})

	return comparisons


def get_linking_settings(target_kg, kg_setting, input_headers, authority_headers):
	comparisons = get_comparison_columns(target_kg, input_headers)

	print("KG SETTING in get linking settings: ", kg_setting)
	print("COMPARISONS: ", comparisons)

	settings = {
		"link_type": "link_only",
		"max_iterations": 20,
		"blocking_rules_to_generate_predictions": get_blocking_rules(kg_setting),
		"comparisons": comparisons,
		"retain_matching_columns": False,
		"retain_intermediate_calculation_columns": False,
		"unique_id_column_name": "unique_id"
	}

	print("\n\n")
	print("SETTINGS:")
	print(settings)
	print("\n\n")

	return settings
