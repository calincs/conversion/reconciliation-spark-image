import string
from pyspark.sql import types
import pyspark.sql.functions as f
from pyspark.sql.functions import lower, col, lit, expr, udf, regexp_extract, split
from pyspark.sql.dataframe import DataFrame
from settings import get_headings


# rather than replacing in place, they add a new column
# the original column will be renamed columnName_original


# register all user defined functions related to data normalization
def register_normalization_udfs(spark):

	dfWhitespace = udf(lambda z: df_whitespace(z))
	spark.udf.register("dfWhitespace", dfWhitespace)

	dfPunctuation = udf(lambda z: df_punctuation(z))
	spark.udf.register("dfPuntuation", dfPunctuation)

	dfNameConcat = udf(lambda z, y, x, w: df_name_concat(z, y, x, w))
	spark.udf.register("dfNameConcat", dfNameConcat)

	dfGender = udf(lambda z: df_gender(z))
	spark.udf.register("dfGender", dfGender)

	return dfWhitespace, dfPunctuation, dfNameConcat, dfGender


# The following functions handle normalizaton functions on the column level

# converts each element in a dataframe column to a lowercase string
def df_lowercase(df: DataFrame, old_col_name: str, new_col_name):
	df = df.withColumn(new_col_name, lower(col(old_col_name)))
	return df


# remove duplicated whitespace, trailing whitespace from a string
def df_whitespace(input_string: str):
	if input_string is not None:
		return " ".join(input_string.split())
	else:
		return input_string


# replaces punctuation in a string with a space
def df_punctuation(input_string: str):
	if input_string is not None:
		input_string.replace("&", " and ")
		translator = str.maketrans('', '', string.punctuation)
		return input_string.translate(translator)
	else:
		return input_string


def df_name_concat(first_name: str, last_name: str, full_name: str, alt_name: str):
	"""
	if alt_name is not null then it returns that value
	concatenates first name and last name with a space
	if those are both null then it inverts the full name order
	"""
	if alt_name is not None:
		return alt_name
	elif first_name is not None and last_name is not None:
		return first_name + " " + last_name
	elif last_name is not None:
		return last_name
	elif first_name is not None:
		return first_name
	elif full_name is not None:
		if "," in full_name:
			name_parts = full_name.split(",")
		elif ";" in full_name:
			name_parts = full_name.split(";")
		else:
			name_parts = full_name.split(" ")
		reordered_name = name_parts[-1]
		if len(name_parts) >= 2:
			for word in name_parts[:-1]:
				reordered_name = reordered_name + " " + word
		return reordered_name
	else:
		return None


def fix_zero_length_strings(df: DataFrame):
	"""Convert any zero length strings or strings that contain only whitespace to a true null
	Args:
		df (DataFrame): Input Spark dataframe
	Returns:
		DataFrame: Spark Dataframe with clean strings

	https://github.com/moj-analytical-services/splink_data_normalisation/blob/master/splink_data_normalisation/fix_string.py
	"""
	string_cols = [item[0] for item in df.dtypes if item[1].startswith('string')]

	stmt = """
	case
	when trim({c}) = '' then null
	else trim({c})
	end
	"""
	for c in string_cols:
		df = df.withColumn(c, expr(stmt.format(c=c)))

	return df


def df_gender(gender: str):
	# translates variations in gender values to F or M
	# 6581072 and 6581097 represent the wikidata entities for male and female
	if gender is not None:
		if "f" in gender.lower() or gender == "1" or "6581072" in gender or "a" == gender:
			return "female"
		elif "m" in gender.lower() or gender == "0" or "6581097" in gender or "b" == gender:
			return "male"
	else:
		return None


def add_missing_columns(target_kg, df_input, df_input_headers):
	headings = get_headings("add_missing", target_kg)
	for heading in headings:
		if heading not in df_input_headers:
			df_input = df_input.withColumn(heading, lit(None).cast(types.StringType()))
	return df_input


# The following functions normalize all columns of a certain type depending on which target KG is specified

def author_normalize(target_kg, df_input, df_input_headers, dfNameConcat):
	# if author first name and last name are provided in input, create an author title out of them
	# only combines them if author_Name_1 or author_Name_2 are empty
	headings = ["author_FirstName", "author_LastName", "author_Name_1", "author_Name_2"]
	for heading in headings:
		if heading not in df_input_headers:
			df_input = df_input.withColumn(heading, lit(None).cast(types.StringType()))
	df_input = df_input.withColumn(
		"author_Name_3", dfNameConcat("author_FirstName", "author_LastName", "author_Name_1", "author_Name_2"))

	# replace authorName2 with results from concatenate step
	# will have kept original value if it was not null
	df_input = df_input.drop("author_Name_2")
	df_input = df_input.withColumnRenamed("author_Name_3", "author_Name_2")

	# delete the extra columns now we are done with them
	df_input = df_input.drop("author_FirstName")
	df_input = df_input.drop("author_LastName")
	return df_input


def dates_normalize(target_kg, df_input, df_input_headers):
	"""
	Date columns converted to only contain years
	"""
	columns = get_headings("date_normalize", target_kg)
	for header in columns:
		if header in df_input_headers:
			df_input = df_input.withColumn(header, regexp_extract(header, r'([1|2][0-9]{3})', 1))
	return df_input


def gender_normalize(target_kg, df_input, dfGender, df_input_headers):
	columns = get_headings("gender_normalize", target_kg)
	for header in columns:
		if header in df_input_headers:
			df_input = df_input.withColumn(header, dfGender(header))
	return df_input


def common_data_normalize(target_kg, df_input, df_lowercase, dfWhitespace, dfPunctuation, df_input_headers):
	columns = get_headings("string_normalize", target_kg)
	# TODO: convert viaf address into id only
	for column in columns:
		if column in df_input_headers:
			# lowercase the string columns
			df_input = df_input.withColumn(column, lower(col(column)))

			# replace punctuation with whitespace
			df_input = df_input.withColumn(column, dfPunctuation(column))

			# remove extra whitespaces
			df_input = df_input.withColumn(column, dfWhitespace(column))

	# convert all zero length strings to nulls
	df_input = fix_zero_length_strings(df_input)

	return df_input


def titles_normalize(target_kg, df_input, df_input_headers):
	# splits workTitle1 on first colon (if present) and adds everything after colon to workTitle3
	columns = get_headings("title_normalize", target_kg)
	if len(columns) == 2:
		if columns[0] in df_input_headers:
			df_input = df_input.withColumn(columns[0], f.split(columns[0], "(?<=^[^:]*)\\:")[0]).\
				withColumn(columns[1], f.split(columns[0], "(?<=^[^:]*)\\:")[1])
	return df_input


def delete_extra_columns(target_kg, df_input, df_input_headers):
	possible_headings = get_headings('accepted_input', target_kg)
	drop_columns = []
	for heading in df_input_headers:
		if heading not in possible_headings:
			drop_columns.append(heading)
	df = df_input.drop(*drop_columns)
	return df


def split_list_columns(target_kg, df_input, df_input_headers):
	# for each column in the authority_config["to_split"]
	# convert the single column containing lists of strings
	# into individual columns of single strings

	print("\nsplitting column lists\n")
	df_input.show(10)

	possible_headings = get_headings('to_split', target_kg)
	for heading in df_input_headers:
		if heading in possible_headings:
			print("heading: ", heading)
			for counter in range(1, 4):
				print("counter: ", counter)
				new_heading = f"{heading}_{counter}"
				print("new heading: ", new_heading)
				#df_input.select([heading][counter-1])
				#df_test = df_input[heading][counter - 1]
				#print("df test")
				#df_test.show(df_test)
				df_input = df_input.withColumn(new_heading, df_input[heading][counter - 1])
	return df_input
