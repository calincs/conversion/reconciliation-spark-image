def get_authority_dataframe(spark, file_path: str):
	# Read data from an S3 bucket
	df = spark.read.option("header", "true").parquet(file_path)
	return df


# Converts input data into a spark dataframe
# Returns false if the input file is an unsuported file type
def get_job_dataframe(spark, job_id: str, input_format: str, path: str):
	input_path = path + "input/" + job_id

	if input_format == "csv":
		df = spark.read.option("header", "true").csv(input_path)
	elif input_format == "tsv":
		df = spark.read.option("header", "true").option("delimiter", "\t").csv(input_path)
	elif input_format == "parquet":
		df = spark.read.option("header", "true").parquet(input_path)
	elif input_format == "json":
		df = spark.read.option("multiLine", "true").option("mode", "PERMISSIVE").json(input_path)
	else:
		return "False"

	print(df.show(10))
	return df


def stopwords():
	# based on the nltk stop word list with some additions based on other common short words found in the data
	# these words won't be used as blocking keys for work titles
	return [
		'i',
		'me',
		'my',
		'we',
		'our',
		'ours',
		'you',
		'your',
		'yours',
		'he',
		'him',
		'his',
		'she',
		'her',
		'hers',
		'it',
		'its',
		'they',
		'them',
		'their',
		'theirs',
		'what',
		'which',
		'who',
		'whom',
		'this',
		'that',
		'these',
		'those',
		'am',
		'is',
		'are',
		'was',
		'were',
		'be',
		'been',
		'being',
		'have',
		'has',
		'had',
		'do',
		'does',
		'did',
		'a',
		'an',
		'the',
		'and',
		'but',
		'if',
		'or',
		'as',
		'until',
		'while',
		'of',
		'at',
		'by',
		'for',
		'with',
		'about',
		'into',
		'through',
		'during',
		'before',
		'after',
		'above',
		'below',
		'to',
		'from',
		'up',
		'down',
		'in',
		'out',
		'on',
		'off',
		'over',
		'under',
		'again',
		'further',
		'then',
		'once',
		'here',
		'there',
		'when',
		'where',
		'why',
		'how',
		'all',
		'any',
		'both',
		'each',
		'few',
		'more',
		'most',
		'other',
		'some',
		'such',
		'no',
		'nor',
		'not',
		'only',
		'own',
		'same',
		'so',
		'than',
		'too',
		'very',
		's',
		't',
		'can',
		'will',
		'just',
		'don',
		'should',
		'now',
		'a',
		'de',
		'la',
		'le',
		'les',
		'dans',
		'et',
		'est',
		'ca',
		'min']
