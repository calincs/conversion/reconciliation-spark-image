from pyspark.sql.functions import udf
from normalize_data import fix_zero_length_strings
from utils import stopwords


# udf_blocking.py #
def register_udfs(spark):
	# https://www.bmc.com/blogs/how-to-write-spark-udf-python/

	secLongestWord = udf(lambda z, y: second_longest_word(z, y))
	spark.udf.register("secLongestWord", secLongestWord)

	longestWord = udf(lambda z: longest_word(z))
	spark.udf.register("longestWord", longestWord)

	firstWord = udf(lambda z, y: first_word(z, y))
	spark.udf.register("firstWord", firstWord)

	lastWord = udf(lambda z: last_word(z))
	spark.udf.register("lastWord", lastWord)

	return secLongestWord, longestWord, firstWord, lastWord


def longest_word(s):
	"""
	return the longest word in string s
	given a tie, it will choose the first lexographically
	"""
	if s:
		word_list = s.split(" ")
		word_list.sort()
		word_list.sort(key=len, reverse=True)
		stop_words = stopwords()
		if len(word_list) == 1:
			return word_list[0]
		elif word_list[0] not in stop_words:
			return word_list[0]
	return None


def second_longest_word(s, tblock1):
	"""
	return the second longest word in string s
	given a tie for first longest word, it will choose the second lexographically
	"""
	if s:
		word_list = s.split(" ")
		word_list.sort()
		word_list.sort(key=len, reverse=True)
		stop_words = stopwords()
		try:
			word = word_list[1]
			if len(word) > 2 and word != tblock1 and word not in stop_words:
				return word
		except:
			if len(word_list[0]) > 2 and word_list[0] != tblock1 and word not in stop_words:
				return word_list[0]
	return None


def last_word(s):
	"""
	returns the last name in a full name. The name must be at least 2 characters long to avoid initials
	"""

	def check_last_word(s_list):
		if len(s_list) > 0:
			if len(s_list[-1]) > 1:
				return s_list[-1]
			else:
				check_last_word(s_list[:-1])
		return None

	if s:
		word_list = s.split(" ")
		return check_last_word(word_list)
	else:
		return None


def first_word(s, ablock1):
	"""
	returns the first name in a full name. The name must be at least 2 characters long to avoid initials
	"""

	def check_first_word(s_list):
		if len(s_list) > 0:
			if len(s_list[0]) > 1 and s_list[0] != ablock1:
				return s_list[0]
			else:
				check_first_word(s_list[1:])
		return None

	if s:
		word_list = s.split(" ")
		return check_first_word(word_list)
	else:
		return None


def blocking(target_kg, df, longestWord, secLongestWord, firstWord, lastWord):
	"""
	adds blocking key columns to the dataframe
	"""

	# title blocking keys
	if target_kg in ["viaf-works", "wikidata-works"]:
		df = df.withColumn('tblock1', longestWord('work_Name_1'))
		df = df.withColumn('tblock2', secLongestWord('work_Name_1', 'tblock1'))
	if target_kg in ["viaf-expressions"]:
		df = df.withColumn('tblock1', longestWord('expression_Name_1'))
		df = df.withColumn('tblock2', secLongestWord('expression_Name_1', 'tblock1'))
	# author blocking keys
	if target_kg in ["viaf-works", "viaf-expressions", "wikidata-works", "wikidata-authors", "getty-people"]:
		df = df.withColumn('ablock1', lastWord('author_Name_1'))
		df = df.withColumn('ablock2', firstWord('author_Name_1', 'ablock1'))

	# convert all zero length strings to nulls
	df = fix_zero_length_strings(df)
	return df
