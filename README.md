# Reconciliation Spark image

This Dockerfile creates the [Apache Spark](https://spark.apache.org/) image that the reconciliation service deploys on the LINCS Kubernetes cluster. It runs Spark 3.0 and contains the python scripts and dependencies needed to perform the data matching step for the reconciliation service.

The API project is located [here](https://gitlab.com/calincs/conversion/reconciliation-service). More detailed documentation on using this image and the full service is in the [API project’s wiki](https://gitlab.com/calincs/conversion/reconciliation-service/-/wikis/home).